Thankyou for purchasing this pack

Direct X, FBX, Unwrap3d formats support Multitrack Named Animations, the same naming convention
as below.

For engines that require the traditional start/stop frame animation numbers, please use the
following reference.



Walk,0,40
IdleA,41,239
IdleB,240,359
GrazeA,360,499
GrazeB,500,639
Trot,640,657
Run,658,670